<?php


use System\Router\Web\Route;

Route::get('/', 'HomeController@index', 'index');
Route::get('create', 'HomeController@create', 'create');