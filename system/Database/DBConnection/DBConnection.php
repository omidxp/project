<?php


namespace System\Database\DBConnection;

use PDO;
use PDOException;

class DBConnection
{
    private static $dbConnection = null;

    private function __construct()
    {
    }

    public static function getDBConnectionInstance()
    {
        if (self::$dbConnection == null) {
            $DBConnection = new DBConnection();
            self::$dbConnection = $DBConnection->dbConnection();

        }
        return self::$dbConnection;
    }

    private function dbConnection()
    {
        $options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC);
        try {
            return new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS,$options);
        } catch (PDOException $exception) {
            echo 'error to connect database with error:' . $exception->getMessage();
        }

    }

}