<?php

namespace System\Router\Api;

class Route
{
    public static function get($url, $executeMethod, $name = null)
    {
        $executeMethod = explode('@', $executeMethod);
        $controllerName = $executeMethod[0];
        $methodName = $executeMethod[1];
        global $routes;
        array_push($routes['get'],
            [
                'url' => 'api/' . trim($url, '/'),
                'class' => $controllerName,
                'method' => $methodName,
                'name' => $name
            ]
        );
    }

    public static function post($url, $executeMethod, $name = null)
    {
        $executeMethod = explode('@', $executeMethod);
        $controllerName = $executeMethod[0];
        $methodName = $executeMethod[1];
        global $routes;
        array_push($routes['post'],
            [
                'url' => 'api/' . trim($url, '/'),
                'class' => $controllerName,
                'method' => $methodName,
                'name' => $name
            ]
        );
    }

}